lib = libInterpreteurExpr
output_directory = out
output_jar = worf.jar

.PHONY: exec

all: $(output_directory) compile run

$(output_directory):
	mkdir -p $(output_directory)

compile: $(srcfiles) $(libfiles)
	javac *.java */*.java -d $(output_directory)
run: $(output_directory)/VueControleur.class
	./start.sh

mrproper:
	rm -rf $(output_directory)
	rm -f compile

