package libNumberLink;

import java.util.Random;

/*                                        *
 *              Generator Class           *
 *                                        *
 */ 

public class Generator {
	// m_pathNumber : The path count
	private int m_pathNumber;
	// m_covered : The number of covered squares
	private int m_covered;
	// m_size : The size of the board
	private int m_size;
	// m_boardSolved : The board with game result at the end, just useful to apply algorithm
	private int[][] m_boardSolved;
	// m_boardUnsolved : The board with blank space to fill
	private int[][] m_boardUnsolved;

	public Generator() {
		m_pathNumber = 0;
		m_covered = 0;
		m_size = 0;
		m_boardSolved = getEmptyBoard();
		m_boardUnsolved = getEmptyBoard();
	}

	// return an empty board filled with 0s
	public int[][] getEmptyBoard() {
		int[][] board = new int[m_size][m_size];
		for (int i = 0; i < m_size; i++) {
			for (int j = 0; j < m_size; j++) {
				board[i][j] = 0;
			}
		}
		return board;
	}

	// Return the number of neighbours added to a path
	// + the number of inaccessible squares, (-1, 0) for example
	public int numUsedNeighbours(int x, int y) {
		int count = 0;
		if (x == 0) {
			count++;
		} else if (m_boardSolved[x - 1][y] != 0) {
			count++;
		}
		if (x == m_size - 1) {
			count++;
		} else if (m_boardSolved[x + 1][y] != 0) {
			count++;
		}
		if (y == 0) {
			count++;
		} else if (m_boardSolved[x][y - 1] != 0) {
			count++;
		}
		if (y == m_size - 1) {
			count++;
		} else if (m_boardSolved[x][y + 1] != 0) {
			count++;
		}
		return count;
	}

	// Return the count of neighbours of cell [k, l] with value `val` 
	public int numSameValuedNeighbours(int x, int y, int val) {
		int count = 0;
		if (x != 0) {
			if (m_boardSolved[x - 1][y] == val) {
				count++;
			}
		}
		if (x != m_size - 1) {
			if (m_boardSolved[x + 1][y] == val) {
				count++;
			}
		}
		if (y != 0) {
			if (m_boardSolved[x][y - 1] == val) {
				count++;
			}
		}
		if (y != m_size - 1) {
			if (m_boardSolved[x][y + 1] == val) {
				count++;
			}
		}
		return count;
	}

	/* Return true if the cell has an isolated cell neighbour 
	 * Changes depending on if it's last node or not
	 * Calculs are the same except lastNode will check if 
	 * the target cell has no more than 1 neighbour of value val
	 */
	public boolean hasIsolatedCells(int x, int y, int val, boolean isLastNode) {
		if (isLastNode) {
			if ((x != 0) && 
			    (m_boardSolved[x - 1][y] == 0) && 
			    (numUsedNeighbours(x - 1, y) == 4) && 
			    (numSameValuedNeighbours(x - 1, y, val) > 1)) 
			{
				return true;
			}
			if ((x != m_size - 1) && 
			    (m_boardSolved[x + 1][y] == 0) && 
			    (numUsedNeighbours(x + 1, y) == 4) && 
			    (numSameValuedNeighbours(x + 1, y, val) > 1)) {
				return true;
			}
			if ((y != 0) && 
			    (m_boardSolved[x][y - 1] == 0) && 
			    (numUsedNeighbours(x, y - 1) == 4) && 
			    (numSameValuedNeighbours(x, y - 1, val) > 1)) {
				return true;
			}
			if ((y != m_size - 1) && 
			    (m_boardSolved[x][y + 1] == 0) && 
			    (numUsedNeighbours(x, y + 1) == 4) && 
			    (numSameValuedNeighbours(x, y + 1, val) > 1)) {
				return true;
			}
		} else {
			if ((x != 0) && 
			    (m_boardSolved[x - 1][y] == 0) && 
			    (numUsedNeighbours(x - 1, y) == 4)) {
				return true;
			}
			if ((x != m_size - 1) && 
			    (m_boardSolved[x + 1][y] == 0) && 
			    (numUsedNeighbours(x + 1, y) == 4)) {
				return true;
			}
			if ((y != 0) && 
			    (m_boardSolved[x][y - 1] == 0) && 
			    (numUsedNeighbours(x, y - 1) == 4)) {
				return true;
			}
			if ((y != m_size - 1) && 
			    (m_boardSolved[x][y + 1] == 0) && 
			    (numUsedNeighbours(x, y + 1) == 4)) {
				return true;
			}
		}
		return false;
	}

	/* Return a neighbour cell and set it's value to val
	 * if it does not make isolated cells
	 */
	public int[] getPathExtensionNeighbour(int x, int y, int val) {
		Random rand = new Random();

		int u, v, xx, yy;
		u = rand.nextInt(4);
		for (v = 0; v < 4; v++) {
			if (u++ == 4) {
				u = 0;
			}
			xx = x;
			yy = y;
			switch (u) {
				case 0:
					if (x == 0) {
						continue;
					}
					xx = x - 1;
					break;
				case 1:
					if (y == m_size - 1) {
						continue;
					}
					yy = y + 1;
					break;
				case 2:
					if (y == 0) {
						continue;
					}
					yy = y - 1;
					break;
				case 3:
					if (x == m_size - 1) {
						continue;
					}
					xx = x + 1;
					break;
			}
			if (m_boardSolved[xx][yy] == 0) {
				if (val != 0) {
					if (numSameValuedNeighbours(xx, yy, val) > 1) {
						continue;
					}
				}
				m_boardSolved[xx][yy] = val;
				if (hasIsolatedCells(x, y, val, false) || hasIsolatedCells(xx, yy, val, true)) {
					m_boardSolved[xx][yy] =  0;
					continue;
				}
				int[] r = {xx, yy};
				return r;
			}
		}
		int[] r = {0, 0};
		return r;
	}

	/* Add a path to m_boardSolved
	 * Add only terminal nodes to m_boardUnsolved
	 */
	public boolean addPath() {
		Random rand = new Random();

		int i = 0, j = 0, s, t, nbr[] = null;
		m_pathNumber++;
		s = rand.nextInt( m_size* m_size);
		for (t = 0; t < m_size * m_size; t++) {
			if (++s == m_size * m_size) {
				s = 0;
			}
			i = s / m_size;
			j = s % m_size;
			if (m_boardSolved[i][j] == 0) {
				m_boardUnsolved[i][j] = m_pathNumber;

				m_boardSolved[i][j] = m_pathNumber;

				if (hasIsolatedCells(i, j, m_pathNumber, true)) {
					m_boardSolved[i][j] =  0;
					m_boardUnsolved[i][j] =  0;

				} else {
					nbr = getPathExtensionNeighbour(i, j, m_pathNumber);
					if (nbr[0] == 0 && nbr[1] == 0) {
						m_boardSolved[i][j] =  0;
						m_boardUnsolved[i][j] =  0;
					} else {
						break;
					}
				}
			}
		}
		if (t == m_size * m_size) {
			m_pathNumber--;
			return false;
		}

		int pathlen = 2;
		m_covered += 2;
		int[] nextNbr;
		while (true) {
			i = nbr[0];
			j = nbr[1];
			nextNbr = getPathExtensionNeighbour(i, j, m_pathNumber);
			if ((nextNbr[0] != 0 || nextNbr[1] != 0) && pathlen <  m_size* m_size) {
				nbr = nextNbr;
			} else {

				m_boardUnsolved[nbr[0]][nbr[1]] = m_pathNumber;

				return true;
			}
			pathlen += 1;
			m_covered += 1;
		}
	}

	// Generate the board, return the unsolved one
	public int[][] generateBoard(int size) {
		this.m_size = size;
		do {
			m_boardUnsolved = getEmptyBoard();
			m_boardSolved = getEmptyBoard();
			m_pathNumber = 0;
			m_covered = 0;
			while (addPath()) {
			}
		} while (m_covered < m_size * m_size);
		return m_boardUnsolved;
	}

	public int getPath() {
		return m_pathNumber;
	}
}
