package libLIB;

public class Position {
	public int x;
	public int y;

	public Position() {
		x = 0;
		y = 0;
	}
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public boolean equals(Position p) {
		return p.x == x && p.y == y;
	}
	
	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
