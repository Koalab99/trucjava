package libLIB;

public class Color {
	private int size;
	private int red[];
	private int green[];
	private int blue[];
	
	public Color(int n) {
		setSize(n);
	}

	public void setSize(int n) {
		size = n;
		red = new int[n];
		green = new int[n];
		blue = new int[n];
		generate();
	}

	public void generate() {
		for(int i = 0; i < size; i++) {
			if(i <= size/3) {
				double pos = i/((double)size/3);
				red[i] = (int)(255.0*(1-pos));
				green[i] = (int)(255.0*pos);
				blue[i] = 0;
			}
			else if(i <= 2*size/3) {
				double pos = i/((double)size/3)-1;
				green[i] = (int)(255.0*(1-pos));
				blue[i] = (int)(255.0*pos);
				red[i] = 0;
			}
			else if(i <= size) {
				double pos = i/((double)size/3)-2;
				blue[i] = (int)(255.0*(1-pos));
				red[i] = (int)(255.0*pos);
				green[i] = 0;
			}
		}
	}
	
	public int getRed(int n) {
		return red[n];
	}
	
	public int getGreen(int n) {
		return green[n];
	}
	
	public int getBlue(int n) {
		return blue[n];
	}
}
