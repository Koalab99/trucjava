import java.util.Observable;
import java.util.Stack;

import libLIB.Position;
import libLIB.Color;
import libLIB.Cardinal;
import libNumberLink.Generator;

public class Modele extends Observable {
	private Generator generator;
	private int[][] grid;
	private int gridSize;
	private int covered;
	private int nbCouleurs;
	private Position[] initPoint;
	private Stack<Position>[] chemins;
	private Color colors;
	private int lastX;
	private int lastY;

	public Modele() {
		generator = new Generator();
		covered = 0;
		System.out.println("Send Changed");
		setChanged();
		notifyObservers();
	}

	public void generate(int n) {
		grid = generator.generateBoard(n);
		gridSize = n;
		nbCouleurs = generator.getPath();
		covered = nbCouleurs;
		colors = new Color(nbCouleurs);
		initPoint = new Position[2*nbCouleurs];
		for(int i = 0, indexInitPoint = 0; i < gridSize * gridSize; i++) {
			int y = i/gridSize;
			int x = i%gridSize;
			if(getColor(x, y) != 0) {
				initPoint[indexInitPoint++] = new Position(x, y);
			}
		}
		chemins = new Stack[nbCouleurs];
		System.out.println("Send Changed");

		setChanged();
		notifyObservers();
	}

	public boolean isInitPoint(int x, int y) {
		for(int i = 0; i < 2*nbCouleurs; i++) {
			if(initPoint[i].x == x && initPoint[i].y == y) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isInitPoint(Position p) {
		for(int i = 0; i < 2*nbCouleurs; i++) {
			if(initPoint[i] == p) {
				return true;
			}
		}
		return false;
	}
	
	public boolean estLibre(int x, int y) {
		return getColor(x, y) == 0;
	}

	public boolean isGameFinished() {
		return (covered == gridSize * gridSize);
	}

	public int getNbrCouleur() {
		return nbCouleurs;
	}

	public int getColor(int x, int y) {
		return grid[y][x];
	}
	
	public boolean hasNeighboursClr(int x, int y, int clr) {
		if(x > 0 && getColor(x-1, y) == clr) 
			return true;
		if(x < gridSize - 1 && getColor(x+1, y) == clr) 
			return true;
		if(y > 0 && getColor(x, y-1) == clr) {
			return true;
		}
		if(y < gridSize - 1 && getColor(x, y+1) == clr)
			return true;
		return false;
	}

	public boolean extendPath(int xx, int yy) {
		if(getColor(lastX, lastY) == 0) {
			return false;
		}
		if(getColor(lastX, lastY) == getColor(xx, yy)) {
			return false;
		}
		int x = lastX;
		int y = lastY;
		System.out.printf("%d %d\n", lastX, lastY);
		if(x == xx && y == yy) {
			return true;
		}
		int clr = getColor(x, y);
		Position pos = new Position(x, y);
		Position tmp = new Position(xx, yy);
		if(!hasNeighboursClr(xx, yy, clr) || (isInitPoint(xx, yy) && chemins[clr-1].get(0) != tmp )) {
			return false;
		}
		
		if(estLibre(xx, yy)) {
			grid[yy][xx] = grid[y][x];
			int couleur = grid[y][x];
			covered++;
			chemins[couleur-1].push(pos);
		}
		else {
			int couleur = grid[yy][xx];
			if(isInitPoint(pos)) {
				return false;
			}
			Position p;
			while(!chemins[couleur-1].isEmpty() && (p = chemins[couleur-1].pop()) != pos) { 
				covered--;
				if(!isInitPoint(p)) {
					grid[p.y][p.x] = 0;
				}
			} 
			int couleurOriginale = grid[y][x];
			chemins[couleurOriginale-1].push(pos);
			grid[pos.y][pos.x] = couleurOriginale;
		}
		System.out.println("Send Changed");

		setChanged();
		notifyObservers();
		return true;
	}
	
	public boolean initPath(int x, int y) {
		if(grid[y][x] == 0) {
			return false;
		}
		else {
			int couleur = grid[y][x];
			Position pos = new Position(x, y);
			chemins[couleur-1] = new Stack<Position>();
			chemins[couleur-1].push(pos);
		}
		setChanged();
		notifyObservers();
		System.out.println("Send Changed");

		return true;
	}

	public boolean shortenPath(int x, int y) {
		int couleur = getColor(x, y);
		if(couleur == 0) {
			return false;
		}
		Position pos = new Position(x, y);
		Position p;
		while((p = chemins[couleur-1].pop()) != pos) {
			covered--;
			if(!isInitPoint(p)) {
				grid[p.y][p.x] = 0;
			}
		}
		covered--;
		grid[p.y][p.x] = 0;
		setChanged();
		notifyObservers();
		System.out.println("Send Changed");

		return true;
	}
	
	public boolean firstClic(int x, int y) {
		Position p = new Position(x, y);
		int couleur = getColor(x, y);
		lastX = x;
		lastY = y;
		if(isInitPoint(x, y)) {
			return initPath(x, y);
		}
		else if(estLibre(x, y)) {
			return false;
		}
		else {
			return shortenPath(x, y);
		}
		
	}
	
	public Cardinal getPreviousNodeDirection(int x, int y) {
		Cardinal ret = Cardinal.NONE;
		int couleur = getColor(x, y);
		Position a, p = new Position(x, y);
		
		if(couleur == 0) {
			return ret;
		}
		Stack<Position> cheminBis = (Stack<Position>)chemins[couleur-1].clone();
		while(cheminBis.pop() != p);
		a = cheminBis.pop();
		if(a.x > p.x) {
			ret = Cardinal.EST;
		}
		else if (a.x < p.x) {
			ret = Cardinal.OUEST;
		}
		else if (a.y > p.y) {
			ret = Cardinal.NORD;
		}
		else if( a.y < p.y) {
			ret = Cardinal.SUD;
		}
		return ret;
	}
	
	public Cardinal getNextNodeDirection(int x, int y) {
		Cardinal ret = Cardinal.NONE;
		int couleur = getColor(x, y);
		Position a, p = new Position(x, y);
		
		if(couleur == 0) {
			return ret;
		}
		Stack<Position> cheminBis = (Stack<Position>)chemins[couleur-1].clone();
		while((a = cheminBis.pop()) != p);
		if(a.x > p.x) {
			ret = Cardinal.EST;
		}
		else if (a.x < p.x) {
			ret = Cardinal.OUEST;
		}
		else if (a.y > p.y) {
			ret = Cardinal.NORD;
		}
		else if( a.y < p.y) {
			ret = Cardinal.SUD;
		}
		return ret;
	}
	
	public Position getPositionFromDirection(int x, int y, Cardinal z) {
		switch(z) {
		case NORD:
			return new Position(x, y+1);
		case SUD:
			return new Position(x, y-1);
		case EST:
			return new Position(x+1, y);
		case OUEST:
			return new Position(x-1, y);
		case NONE:
			break;
		}
		return new Position(x, y);
	}
	
	public Position getPositionFromDirection(Position p, Cardinal z) {
		int x = p.x;
		int y = p.y;
		return getPositionFromDirection(x, y, z);
	}
	
	public int getRed(int n) {
		if(n == 0) {
			return 0;
		}
		return  colors.getRed(n-1);
	}
	
	public int getGreen(int n) {
		if(n == 0) {
			return 0;
		}
		return colors.getGreen(n-1);
	}
	
	public int getBlue(int n) {
		if(n == 0) {
			return 0;
		}
		return colors.getBlue(n-1);
	}

}