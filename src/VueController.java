import java.util.Observable;
import java.util.Observer;
import javafx.application.Application;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;

import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;

import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.shape.Rectangle;

public class VueController extends Application implements Observer {
    Modele m;
    
    Rectangle[][] tabRect;
    int taille;
    BorderPane border;
    GridPane gPane;
    
    @Override
    public void start(Stage primaryStage) {

        // initialisation du modèle que l'on souhaite utiliser
    	taille = 5;
        m = new Modele();
        m.generate(taille);
        
        tabRect = new Rectangle[taille][taille];

        // gestion du placement (permet de palcer le champ Text affichage en haut, et GridPane gPane au centre)
         border = new BorderPane();

        // permet de placer les diffrents boutons dans une grille
        gPane = new GridPane();

        // la vue observe les "update" du modèle, et réalise les mises à jour graphiques
        m.addObserver(this);
        
    	gPane.setGridLinesVisible(true);
    	
    	border.setCenter(gPane);
    	
    	Scene scene = new Scene(border, Color.LIGHTBLUE);
    	
    	primaryStage.setTitle("Ligne de vie");
	    primaryStage.setScene(scene);
	    primaryStage.show();
	    update(m, new Object());

            
    }
	
	@Override
	public void update(Observable o, Object arg) {
		System.out.println("DEBUG");
		for (int column = 0; column < 5; column++) {
		    for (int row = 0; row < 5; row++) {
		
		        final int fColumn = column;
		        final int fRow = row;
		
		        int color = m.getColor(row,column);
		        Rectangle r = new Rectangle(50,50);
		        r.setFill(Color.rgb(m.getRed(color),m.getGreen(color),m.getBlue(color)));
		        tabRect[row][column] = r;
		
		
		        r.setOnDragDetected(new EventHandler<MouseEvent>() {
		            public void handle(MouseEvent event) {
		            	m.firstClic(fRow, fColumn);
		                Dragboard db = r.startDragAndDrop(TransferMode.ANY);
		                ClipboardContent content = new ClipboardContent();
		                content.putString(""); // non utilisé actuellement
		                db.setContent(content);
		                event.consume();
		            }
		        });
	
				r.setOnDragEntered(new EventHandler<DragEvent>() {
				    public void handle(DragEvent event) {
				    	m.extendPath(fRow, fColumn);
				        event.consume();
				    }
				});
				
				r.setOnDragDone(new EventHandler<DragEvent>() {
				    public void handle(DragEvent event) {
				
			        }
			    });
			
				gPane.add(tabRect[row][column], row, column);
		    }
		}
	}
	
    public static void main(String[] args) {
        launch(args);
    }

}
